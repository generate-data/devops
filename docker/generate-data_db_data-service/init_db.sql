create role master_data_service with login encrypted password 'master_data_service';
create database master_data_service_db owner master_data_service template template0 encoding 'UTF8';
grant all privileges on database master_data_service_db to master_data_service;
