#!/bin/bash

###############################################################################################################
echo ":: Perform git submodule update --init --recursive"

git submodule update --init --recursive
