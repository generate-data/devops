#!/bin/bash
DEPLOYMENT_NODE=DEPLOYMENT_NODE_ENV
IMAGE_NAME=IMAGE_NAME_ENV
IMAGE_VERSION=IMAGE_VERSION_ENV

docker build -t .../$IMAGE_NAME:$IMAGE_VERSION -f Dockerfile_$IMAGE_NAME .
